package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test 
	//Test User Story 1
	public void testObstacleAndInitialization() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(2,3);
		boolean obstacle1 = rover.planetContainsObstacleAt(4,7);
		
		assertTrue(obstacle);
		assertTrue(obstacle1);
	}

	@Test
	//Test User Story 2
	public void testRoverLandingStatus() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	//Test User Story 3
	public void testRotationRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	//Test User Story 3
	public void testRotationLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	//Test User Story 4
	public void testMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,6)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	//Test User Story 5
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		rover.setRoverStartingPosition("(5,8,E)");
		
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	//Test User Story 6
	public void testMultipleMoves() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	//Test User Story 7
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	//Test User Story 8
	public void testObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
	
	@Test
	//Test User Story 9
	public void testMultipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	//Test User Story 10
	public void testObstacleAndWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	//Test User Story 11
	public void testObstacleWrappingAndTurnAroundThePlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
	
}
