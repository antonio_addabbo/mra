package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	
	private int planetX = 0;
	private int planetY = 0;
	private List<String> planetObstacles;
	private String rover;
	

	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = new ArrayList<String>(planetObstacles);
		this.rover = "(0,0,N)";
		
	}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean check=false;
		if(x < 0 || y < 0)
		{
			throw new MarsRoverException("Coordinate X or Y are below 0");
		}
		else
		{
			String coordinate = "("+x+","+y+")";
			
			for(int i=0 ; i<planetObstacles.size();i++)
			{
				if(planetObstacles.get(i).compareToIgnoreCase(coordinate) == 0)
				{
					check = true;
				}
			}
		}
		
		return check;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	
	public String executeCommand(String commandString) throws MarsRoverException {
		String coordinateTemp,TEST;
		ArrayList <String> obstacle = new ArrayList<String>();
		int coordinateX;
		int coordinateY;
		boolean flag=true;
		for(int i=0; i<commandString.length();i++)
			{
				coordinateTemp = checkCommand(commandString.substring(i, i+1));
				coordinateX = Integer.parseInt(coordinateTemp.substring(1, 2));
				coordinateY = Integer.parseInt(coordinateTemp.substring(3, 4));
				if(planetContainsObstacleAt(coordinateX, coordinateY))
				{
					for(int k=0; k<obstacle.size();k++)
					{
						if(obstacle.get(k).compareTo(coordinateTemp.substring(0, 4) + ")") == 0)
						{
							TEST = coordinateTemp.substring(0, 4) + ")";
							flag = false;
							break;
						}
						TEST = coordinateTemp.substring(0, 4) + ")";
					}
					
					if(flag) 
						{
							obstacle.add(coordinateTemp.substring(0, 4) + ")");
						}
					flag = true;
				}
				else
				{
					this.rover = checkCommand(commandString.substring(i, i+1));
				}
			}
		for(int k=0; k<obstacle.size();k++)
		{
			this.rover += obstacle.get(k);
		}
			return this.rover;
		}
	
	public void setRoverStartingPosition(String coordinate)
	{
		this.rover = coordinate;
	}
	
	public String rotateRight()
	{
		String direction = rover.substring(5, 6);
		String coordinate = rover.substring(0, 5);
		String[] directions = {"N","E","S","W"};
			for(int i=0; i<4 ; i++)
			{
					if(i==3 && directions[i].compareToIgnoreCase(direction) == 0)
					{
						direction = directions[0];
						break;
					}
					else if(directions[i].compareToIgnoreCase(direction) == 0)
					{
						direction = directions[i+1];
						break;
					}	
			}		
		return coordinate+direction+")";
	}
	
	public String rotateLeft()
	{
		String direction = rover.substring(5, 6);
		String coordinate = rover.substring(0, 5);
		String[] directions = {"N","E","S","W"};
			for(int i=0; i<4 ; i++)
			{
					if(i==0 && directions[i].compareToIgnoreCase(direction) == 0)
					{
						direction = directions[3];
						break;
					}
					else if(directions[i].compareToIgnoreCase(direction) == 0)
					{
						direction = directions[i-1];
						break;
					}	
			}
		return coordinate+direction+")";
	}
	
	public String forward()
	{
		String direction = rover.substring(5, 6);
		int coordinateX = Integer.parseInt(rover.substring(1, 2));
		int coordinateY = Integer.parseInt(rover.substring(3, 4));
		String[] directions = {"N","E","S","W"};
			if(direction.compareTo(directions[0]) == 0)
			{
				if(coordinateY != planetY-1)	coordinateY++;
				else coordinateY=0;
			}
			else if(direction.compareTo(directions[1]) == 0)
			{
				if(coordinateX != planetX-1)	coordinateX++;
				else coordinateY=0;
			}
			else if(direction.compareTo(directions[2]) == 0)
			{
				if(coordinateY !=0)	coordinateY--;
				else coordinateY=planetY-1;
			}
			else if(direction.compareTo(directions[3]) == 0)
			{
				if(coordinateX !=0)	coordinateX--;
				else coordinateX=planetX-1;
			}
		return "("+coordinateX+","+coordinateY+","+direction+")";
	}
	
	public String backward()
	{
		String direction = rover.substring(5, 6);
		int coordinateX = Integer.parseInt(rover.substring(1, 2));
		int coordinateY = Integer.parseInt(rover.substring(3, 4));
		String[] directions = {"N","E","S","W"};
			if(direction.compareTo(directions[0]) == 0)
			{
				if(coordinateY != 0)	coordinateY--;
				else coordinateY=planetY-1;
			
			}
			else if(direction.compareTo(directions[1]) == 0)
			{
				if(coordinateX != 0)	coordinateX--;
				else coordinateX=planetX-1;
			}
			else if(direction.compareTo(directions[2]) == 0)
			{
				if(coordinateY !=planetY-1)	coordinateY++;
				else coordinateY=0;
			}
			else if(direction.compareTo(directions[3]) == 0)
			{
				if(coordinateX !=planetX-1)	coordinateX++;
				else coordinateX=0;
			}
		return "("+coordinateX+","+coordinateY+","+direction+")";
	}
	
	public String checkCommand(String commandString) throws MarsRoverException
	{
		if(commandString == "")
		{
			return rover;
		}
		else if (commandString.compareToIgnoreCase("r") == 0) 
		{
			return rotateRight();
		}
		else if (commandString.compareToIgnoreCase("l") == 0) 
		{
			return rotateLeft();
		}
		else if (commandString.compareToIgnoreCase("f") == 0) 
		{
			return forward();
		}
		else if(commandString.compareToIgnoreCase("b") == 0)
		{
			return backward();
		}
		else 
		{
			throw new MarsRoverException("Invalid command. Retry.");
		}
	}
	
}
